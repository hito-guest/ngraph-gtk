# ngraph-gtk debian package

debian package of ngraph-gtk

Ngraph is the program to create scientific 2-dimensional graphs for researchers and engineers. This program can create advanced graphs which can't be created by spreadsheets. Graphs can be exported to postscript.

Homepage: http://hito.music.coocan.jp/ngraph/ngraph-gtk.html
